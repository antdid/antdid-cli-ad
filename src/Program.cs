﻿using System.Diagnostics;
using Antdid.Cli.Ad.Commands;

namespace Antdid.Cli.Ad
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var app = Cli.Init(new CliInfo(ThisCli.Name, ThisCli.ToolCommandName, ThisCli.Version, ThisCli.Description));

            var sw1 = Stopwatch.StartNew();
            var shortcutManagerCommand = new ShortcutManagerCommand();
            app.AddSubcommand(shortcutManagerCommand);
            sw1.Stop();

            var sw2 = Stopwatch.StartNew();
            foreach (var shortcut in shortcutManagerCommand.ListShortcuts)
            {
                var shortcutCommand = new ShortcutCommand(shortcut)
                {
                    Parent = app
                };
                app.AddSubcommand(shortcutCommand);
            }
            sw2.Stop();

            Perf.AddTime("shortcutManagerCommand", sw1, true);
            Perf.AddTime("shortcuts", sw2, true);

            return Cli.Run(app, args);
        }
    }
}
