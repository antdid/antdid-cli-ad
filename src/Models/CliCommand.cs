namespace Antdid.Cli.Ad.Models
{
    internal class CliCommand
    {
        public string Command { get; set; }
        public string? Arguments { get; set; }
        public bool ShowOutput { get; set; } = true;
        public bool UseAnyArguments { get; set; } = true;
        public string? OptionToDisable { get; set; }
        public List<string>? UseOptions { get; set; }

        public CliCommand(string command)
        {
            Command = command;
        }

        public string GetDescription() => string.IsNullOrWhiteSpace(Arguments) ? Command : string.Join(" ", new[] { Command, Arguments });
    }
}
