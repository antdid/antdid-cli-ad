using McMaster.Extensions.CommandLineUtils;

namespace Antdid.Cli.Ad.Models
{
    internal class Option
    {
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string Description { get; set; }
        public bool Required { get; set; }
        public CommandOptionType Type { get; set; }

        public Option(string shortName, string longName, string description, bool required, CommandOptionType type)
        {
            ShortName = shortName;
            LongName = longName;
            Description = description;
            Required = required;
            Type = type;
        }
    }
}
