namespace Antdid.Cli.Ad.Models
{
    internal class Shortcut
    {
        public string Name { get; set; }
        public List<string>? Names { get; set; }
        public string? Description { get; set; }
        public bool ShowInHelp { get; set; } = true;
        public List<Option>? Options { get; set; }
        public List<CliCommand>? Commands { get; set; }
        public List<Shortcut>? Shortcuts { get; set; }

        public Shortcut(string name)
        {
            Name = name;
        }
    }
}
