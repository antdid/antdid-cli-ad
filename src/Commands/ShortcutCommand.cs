using System.Text;
using Antdid.Cli.Ad.Models;
using Antdid.Cli.Helpers;
using Antdid.Cli.Services;
using McMaster.Extensions.CommandLineUtils;

namespace Antdid.Cli.Ad.Commands
{
    internal class ShortcutCommand : CliCommand
    {
        private readonly IProcessService _processService;

        public ShortcutCommand(Shortcut shortcut, IProcessService? processService = null)
        {
            _processService = processService ?? new ProcessService();
            var cmd = this;
            cmd.Name = shortcut.Name;

            if (shortcut.Names != null && shortcut.Names.Any())
            {
                shortcut.Names.ForEach(cmd.AddName);
            }

            cmd.Description = shortcut.Description;
            cmd.ShowInHelpText = shortcut.ShowInHelp;
            CommandOption? optionHelp = null;
            var hasNoCommand = shortcut.Commands == null || shortcut.Commands.Count == 0;
            var onlyOneCommand = shortcut.Commands != null && shortcut.Commands.Count == 1;

            if (onlyOneCommand)
            {
                var command = shortcut.Commands!.First();
                string descriptionCmd = command.GetDescription();

                optionHelp = cmd.Option<bool>("-hh|--hhelp", $"Show help for the `{descriptionCmd}` command.", CommandOptionType.NoValue);

                if (string.IsNullOrWhiteSpace(cmd.Description))
                {
                    cmd.Description = $"Shortcut for the `{descriptionCmd}` command.";
                }
            }
            else if (!hasNoCommand)
            {
                AddDescriptionSequenceCommands(shortcut, cmd);
            }

            if (shortcut.Options != null && shortcut.Options.Any())
            {
                foreach (var option in shortcut.Options)
                {
                    var opt = cmd.Option($"{option.ShortName}|{option.LongName}", option.Description, option.Type);
                    if (option.Required)
                    {
                        opt.IsRequired();
                    }
                }
            }

            if (shortcut.Shortcuts != null && shortcut.Shortcuts.Any())
            {
                foreach (var sc in shortcut.Shortcuts)
                {
                    var shortcutCommand = new ShortcutCommand(sc, _processService)
                    {
                        Parent = cmd
                    };
                    cmd.AddSubcommand(shortcutCommand);
                }
            }

            cmd.OnExecute(() =>
            {
                bool? success = null;

                if (shortcut.Commands != null && shortcut.Commands.Any())
                {
                    var showOptionHelp = optionHelp != null && optionHelp.HasValue();
                    //Recuperation des autres options inconnus
                    var haveRemainingArgs = cmd.RemainingArguments != null && cmd.RemainingArguments.Count > 0;

                    var remainingArguments = string.Empty;
                    if (!showOptionHelp && haveRemainingArgs)
                    {
                        remainingArguments += " " + ArgumentEscaper.EscapeAndConcatenate(cmd.RemainingArguments!);
                    }

                    var successCmds = true;
                    foreach (var c in shortcut.Commands)
                    {
                        if (!string.IsNullOrWhiteSpace(c.OptionToDisable))
                        {
                            var option = cmd.Options.Where(o => c.OptionToDisable.EndsWith(o.ShortName!) || c.OptionToDisable.EndsWith(o.LongName!)).FirstOrDefault();
                            if (option != null && option.OptionType == CommandOptionType.NoValue && option.HasValue())
                            {
                                continue;
                            }
                        }
                        if (c.UseOptions != null && c.UseOptions.Any())
                        {
                            foreach (var opt in c.UseOptions)
                            {
                                var option = cmd.Options.Where(o => opt.EndsWith(o.ShortName!) || opt.EndsWith(o.LongName!)).FirstOrDefault();
                                if (option != null && option.HasValue())
                                {
                                    c.Arguments += $" {opt} \"{option.Value()}\"";
                                }
                            }
                        }
                        if (c.UseAnyArguments)
                        {
                            c.Arguments += remainingArguments;
                        }
                        if (showOptionHelp)
                        {
                            c.Arguments = "--help";
                        }
                        successCmds = successCmds && _processService.Run(c.Command, c.Arguments!, c.ShowOutput);
                    }
                    success = successCmds;
                }
                else
                {
                    return Cli.ExecuteHelp(cmd);
                }

                ConsoleHelper.Result(success);
                return !success.HasValue ? 0 : success.Value ? 0 : 1;
            });
        }

        private static void AddDescriptionSequenceCommands(Shortcut shortcut, ShortcutCommand cmd)
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(cmd.Description))
            {
                sb.AppendLine();
                sb.AppendLine();
            }

            sb.AppendLine("Sequence of commands:");
            var commandsDescriptions = shortcut.Commands!.Select(c => c.GetDescription()).ToList();
            commandsDescriptions.ForEach(desc => sb.AppendLine("- " + desc));
            cmd.Description += sb.ToString();
        }
    }
}
