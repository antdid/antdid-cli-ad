using System.Diagnostics;
using System.Text.Json;
using Antdid.Cli.Ad.Json.Contexte;
using Antdid.Cli.Ad.Models;
using Antdid.Cli.Extensions;
using Antdid.Cli.Helpers;
using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;
using Spectre.Console.Json;

namespace Antdid.Cli.Ad.Commands
{
    internal class ShortcutManagerCommand : CliCommand
    {
        public string? JsonShortcuts { get; internal set; }
        public List<Shortcut> ListShortcuts { get; internal set; } = new();

        public ShortcutManagerCommand()
        {
            Init();

            var cmd = this;
            cmd.Name = "shortcut";
            cmd.AddName("sc");
            cmd.Description = "Managing shortcuts";

            cmd.Command("view", cmdV =>
            {
                cmdV.AddName("v");
                cmdV.Description = "See the detail of a shortcut (json)";
                var argName = cmdV.Argument("name", "Shortcut name", true).IsRequired();

                cmdV.OnExecute(() =>
                {
                    var foundShortcut = FindShortcut(argName, ListShortcuts);
                    if (foundShortcut != null)
                    {
                        Console.WriteLine(foundShortcut.ToJson());
                    }
                    else
                    {
                        ShortcutNotFound(argName);
                    }
                    return 0;
                });
            });
            cmd.Command("list", cmdL =>
            {
                cmdL.AddName("l");
                cmdL.Description = "See the list of shortcuts (json)";
                cmdL.OnExecute(() =>
                {
                    AnsiConsole.Write(new JsonText(JsonShortcuts!).BracesColor(Color.Aqua).BracketColor(Color.Fuchsia).CommaColor(Color.Wheat1));
                    return 0;
                });
            });
            cmd.Command("delete", cmdD =>
            {
                cmdD.AddName("d");
                cmdD.Description = "Delete a shortcut";
                var argName = cmdD.Argument("name", "Shortcut name", true);
                var optRemoveAll = cmdD.Option("-a|--all", "Remove all shortcuts", CommandOptionType.NoValue);
                cmdD.OnExecute(() =>
                {
                    bool? success = null;
                    var removeAll = optRemoveAll.HasValue();
                    if (removeAll)
                    {
                        var sure = Prompt.GetYesNo("Are you sure you want to delete all shortcuts?", true);
                        if (sure)
                        {
                            ListShortcuts.Clear();
                            JsonShortcuts = ListShortcuts.ToJson();
                            File.WriteAllText(Constant.PathFileShortcut, JsonShortcuts);
                            success = true;
                        }
                    }
                    else if (argName.HasValue)
                    {
                        var foundShortcut = FindShortcut(argName, ListShortcuts);
                        if (foundShortcut != null)
                        {
                            DeleteShortcut(foundShortcut, ListShortcuts);
                            JsonShortcuts = ListShortcuts.ToJson();
                            File.WriteAllText(Constant.PathFileShortcut, JsonShortcuts);
                            success = true;
                        }
                        else
                        {
                            ShortcutNotFound(argName);
                        }
                    }
                    else
                    {
                        cmdD.ShowHelp();
                    }
                    ConsoleHelper.Result(success);
                    return !success.HasValue ? 0 : success.Value ? 0 : 1;
                });
            });
            cmd.Command("restore", cmdR =>
            {
                cmdR.AddName("r");
                cmdR.Description = "Restore the list of default shortcuts";
                cmdR.OnExecute(() =>
                {
                    var success = false;
                    try
                    {
                        File.Replace(Constant.PathFileShortcutBase, Constant.PathFileShortcut, null);
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        ConsoleHelper.Debug(ex.Message);
                    }
                    ConsoleHelper.Result(success);
                    return success ? 0 : 1;
                });
            });
            cmd.Command("import", cmdI =>
            {
                cmdI.AddName("i");
                cmdI.Description = "Import shortcuts from a file (json)";
                var argPath = cmdI.Argument<string>("filePath", "Filepath to import (json)", false).IsRequired();
                cmdI.OnExecute(() =>
                {
                    var success = false;
                    var fileInfo = new FileInfo(argPath.ParsedValue);
                    if (fileInfo.Exists)
                    {
                        if (Constant.JsonExtension.Equals(fileInfo.Extension?.ToLowerInvariant()))
                        {
                            var json = File.ReadAllText(fileInfo.FullName);
                            try
                            {
                                var obj = json.ToObject<List<Shortcut>>();
                                var count = obj?.Count ?? 0;
                                if (count > 0)
                                {
                                    var isValid = Validate(obj!);
                                    if (isValid)
                                    {
                                        File.WriteAllText(Constant.PathFileShortcut, obj.ToJson());
                                        ConsoleHelper.Success($"The shortcuts configuration file now contains {count} shortcut(s).");
                                        success = true;
                                    }
                                    else
                                    {
                                        ConsoleHelper.Error("The shortcut list is not valid.");
                                    }
                                }
                                else
                                {
                                    ConsoleHelper.Error("The shortcut list is empty.");
                                }

                            }
                            catch (JsonException jex)
                            {
                                ConsoleHelper.Error("Error json format.");
                                ConsoleHelper.Debug(jex.Message);
                            }
                        }
                        else
                        {
                            ConsoleHelper.Error($"The file extension `{fileInfo.Extension}` is invalid.");
                        }
                    }
                    else
                    {
                        ConsoleHelper.Error($"The `{argPath.Value}` file does not exist.");
                    }
                    ConsoleHelper.Result(success);
                    return success ? 0 : 1;
                });
            });
            cmd.OnExecute(() =>
            {
                cmd.ShowHelp();
            });
        }

        #region Private

        private static void ShortcutNotFound(CommandArgument argName) => Console.WriteLine($"The shortcut named `{string.Join(" ", argName.Values)}` was not found.");

        private bool Validate(List<Shortcut> objs)
        {
            //Check name is unique
            var names = objs.Where(s => !string.IsNullOrWhiteSpace(s.Name)).Select(s => s.Name).ToList();
            var allNames = new List<string>();
            allNames.AddRange(names);
            allNames.AddRange(objs.Where(s => s.Names != null && s.Names.Any()).SelectMany(s => s.Names!).ToList());
            var isValid = names.Count == objs.Count && allNames.Distinct(StringComparer.InvariantCultureIgnoreCase).Count() == allNames.Count;

            if (isValid)
            {
                foreach (var obj in objs)
                {
                    if (obj.Shortcuts != null && obj.Shortcuts.Count > 0)
                    {
                        isValid = Validate(obj.Shortcuts);
                    }
                    else
                    {
                        isValid = obj.Commands != null && obj.Commands.Count > 0 && !obj.Commands.Any(s => string.IsNullOrWhiteSpace(s.Command));
                    }

                    if (!isValid)
                    {
                        break;
                    }
                }
            }

            return isValid;
        }

        private void Init()
        {
            JsonShortcuts = File.ReadAllText(Constant.PathFileShortcut);

            var sw = Stopwatch.StartNew();
            var opt = JsonHelper.GetJsonSerializerOptions();
            opt.TypeInfoResolver = ShortcutsContexte.Default;

            ListShortcuts = JsonShortcuts.ToObject<List<Shortcut>>(opt) ?? new();

            sw.Stop();

            Perf.AddTime("shortcutManagerCommand json serialization", sw);
        }

        private static Shortcut? FindShortcut(CommandArgument argName, List<Shortcut> shortcuts)
        {
            Shortcut? foundShortcut = null;
            foreach (var shortcutName in argName.Values)
            {
                foundShortcut = shortcuts.FirstOrDefault(s => s.Name.Equals(shortcutName, StringComparison.InvariantCultureIgnoreCase));
                if (foundShortcut != null)
                {
                    shortcuts = foundShortcut.Shortcuts ?? new();
                }
            }

            return foundShortcut;
        }

        private void DeleteShortcut(Shortcut shortcutToDelete, List<Shortcut> shortcuts)
        {
            var removed = shortcuts.Remove(shortcutToDelete);
            if (!removed)
            {
                foreach (var shortcut in shortcuts)
                {
                    if (shortcut.Shortcuts != null && shortcut.Shortcuts.Any())
                    {
                        DeleteShortcut(shortcutToDelete, shortcut.Shortcuts);
                    }
                }
            }
        }

        #endregion Private
    }
}
