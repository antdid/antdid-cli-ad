﻿using System.Text.Json.Serialization;
using Antdid.Cli.Ad.Models;

namespace Antdid.Cli.Ad.Json.Contexte
{
    [JsonSourceGenerationOptions(WriteIndented = true, DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull, PropertyNamingPolicy = JsonKnownNamingPolicy.CamelCase)]
    [JsonSerializable(typeof(List<Shortcut>))]
    internal partial class ShortcutsContexte : JsonSerializerContext
    {
    }
}
