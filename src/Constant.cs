namespace Antdid.Cli.Ad
{
    public static class Constant
    {
        public static readonly string PathFileShortcutBase = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", "shortcuts.base.json");
        public static readonly string PathFileShortcut = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data", "shortcuts.json");
        public static readonly string JsonExtension = ".json";
    }
}
