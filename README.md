# Antdid.Cli.Ad

.NET CLI tool to make shortcuts and sequence of commands

## Installation

Download the [.NET SDK](https://dotnet.microsoft.com/download/dotnet/current) or later.

Add the project registry package as a nuget source
```
dotnet nuget add source "https://gitlab.com/api/v4/projects/40800082/packages/nuget/index.json" --name "gitlab-antdid"
```

.NET Global Tool, using the command-line:

```
dotnet tool update -g antdid-cli-ad
```

## Usage

```
.NET CLI tool to make shortcuts and sequence of commands

Usage: ad [command] [options]

Options:
  -?|-h|--help  Show help information.

Commands:
  commit        Sequence of commands:
                - git add .
                - git commit
                - git pull -r
                - git push

  d             Shortcut for the `docker` command.
  dc            Shortcut for the `docker-compose` command.
  g             Shortcut for the `git` command.
  kub           Shortcut for the `kubectl` command.
  net           Shortcut for the `dotnet` command.
  shortcut      Managing shortcuts
  to            Shortcut for the `adt` command.

Run 'ad [command] -?|-h|--help' for more information about a command.
```

## Reserved names

- shortcut
- sc
- complete
